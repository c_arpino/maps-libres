/* @preserve
 * @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt GNU Affero General Public License, version 3
 */

// Ícone para todos POI (ponto de interesse)
var nodo = L.icon({
  iconUrl: './img/marker-icon-regosh.svg',
  shadowUrl: './leaflet/images/marker-shadow.png',
  iconSize:     [28, 41], // size of the icon
  shadowSize:   [68, 82], // size of the shadow
  iconAnchor:   [12, 41], // point of the icon which will correspond to marker's location
  shadowAnchor: [22, 82],  // the same for the shadow
  popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
});

var nodos = L.layerGroup();

// Nodo POA
var nodopoa_popup = '<a class="nodo_link" href="https://tecnologias.libres.cc/regosh-organizacao/nodos/nodo-poa-br/wikis/home">' +
'<img class="nodo_imagem" src="./img/icone-nodo-poabr.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Porto Alegre - BR</div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodopoa_coords = [-30.0728474, -51.1193707];
var nodopoa = L.marker(
    nodopoa_coords,
    {icon: nodo}
)
.bindPopup(nodopoa_popup)
.addTo(nodos);

// Nodo ABC Paulista
var nodoabc_popup = '<a class="nodo_link" href="https://tecnologias.libres.cc/regosh-organizacao/nodos/lablivre">' +
'<img class="nodo_imagem" src="https://tecnologias.libres.cc/regosh-organizacao/nodos/lablivre/web-lablivre/wikis/uploads/da6b6e82a9d271159e76ec0943f4d5ea/Logo_LabLivre_AF_808x602.png" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">ABC Paulista - BR</div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodoabc_coords = [-23.67713, -46.56489];
var nodoabc = L.marker(
    nodoabc_coords,
    {icon: nodo}
)
.bindPopup(nodoabc_popup)
.addTo(nodos);

// Nodo Buenos Aires
var nodoba_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Buenos Aires - AR</div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodoba_coords = [-34.6158037, -58.5033382];
var nodoba = L.marker(
    nodoba_coords,
    {icon: nodo}
)
.bindPopup(nodoba_popup)
.addTo(nodos);

// Nodo Mendonza
var nodomz_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Mendonza - AR</div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodomz_coords = [-32.883334, -68.8760287];
var nodomz = L.marker(
    nodomz_coords,
    {icon: nodo}
)
.bindPopup(nodomz_popup)
.addTo(nodos);

// Nodo Santiago
var nodost_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Santiago - CL</div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodost_coords = [-33.4727092, -70.7699147];
var nodost = L.marker(
    nodost_coords,
    {icon: nodo}
)
.bindPopup(nodost_popup)
.addTo(nodos);

// Nodo Lima
var nodolm_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Lima - PE </div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodolm_coords = [-12.0266034, -77.127864];
var nodolm = L.marker(
    nodolm_coords,
    {icon: nodo}
)
.bindPopup(nodolm_popup)
.addTo(nodos);

// Nodo Quito
var nodoqt_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Quito - EQ </div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodoqt_coords = [-0.1865938, -78.5706232];
var nodoqt = L.marker(
    nodoqt_coords,
    {icon: nodo}
)
.bindPopup(nodoqt_popup)
.addTo(nodos);

// Nodo Medellin
var nodoml_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Medellin - CO </div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodoml_coords = [6.2442023, -75.616231];
var nodoml = L.marker(
    nodoml_coords,
    {icon: nodo}
)
.bindPopup(nodoml_popup)
.addTo(nodos);

// Nodo Cidade do México
var nodomx_popup = '<a class="nodo_link" href="#">' +
'<img class="nodo_imagem" src="./img/icone-redondo-regosh.svg" />' +
'<div class="nodo">' +
'<div class="nodo_titulo">Nodo REGOSH</div>' +
'<div class="nodo_nome">Cidade do México - MX </div>' +
'<div class="nodo_descricao">Ningu&eacute;m escreveu uma descri&ccedil;&atilde;o ainda</div>' +
'<div class="nodo_atividade">&Uacute;ltima atividade hoje</div>' +
'</div>' +
'</a>';
var nodomx_coords = [19.39068, -99.2836971];
var nodomx = L.marker(
    nodomx_coords,
    {icon: nodo}
)
.bindPopup(nodomx_popup)
.addTo(nodos);

var atribuicao = 'Dados de mapas &copy; contribuidora(e)s do ' +
    '<a href="https://www.openstreetmap.org/">OpenStreetMap</a>, ' +
    '<a href="https://wiki.openstreetmap.org/wiki/Standard_tile_layer">Standard Tiles</a> ' +
    '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>. ' +
    'Mapa feito com ' +
    '<a href="https://regosh.libres.cc/pt/">&hearts;</a>' +
    ', ' +
    '<a href="https://matehackers.org/">&#9398;</a>' +
    ' e ' +
    '<a href="https://tecnologias.libres.cc/iuriguilherme/maps.libres.cc">liberdade</a>' +
    ' por ' +
    '<a href="https://tecnologias.libres.cc/regosh-organizacao/nodos/nodo-poa-br/wikis/home">REGOSH Nodo POA-BR</a>' +
    '.' +
    'Problemas com o mapa&quest; <a href="https://www.openstreetmap.org/fixthemap">Ajude a consertar&excl;</a>';

/* @preserve https://operations.osmfoundation.org/policies/tiles/ */
var tiles_url = 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';

var tiles = L.layerGroup();

var standard = L.tileLayer(
    tiles_url,
    {id: 'standard', attribution: atribuicao}
).addTo(tiles);

var regosh = L.map('mapa_regosh', {
  center: [-13.6912, -60.710379],
  zoom: 3,
  maxZoom: 18,
  zoomControl: false,
  layers: [
    tiles,
    nodos
  ]
});

var baseLayers = {
  "Todas": tiles,
  "OSM Standard Tiles": standard
};

var overlays = {
  "Nodos REGOSH": nodos
};

new L.control.layers(baseLayers, overlays).addTo(regosh);

new L.control.zoom({position: 'bottomleft'}).addTo(regosh);

var popup = L.popup();

function onMapClick(e) {
  popup
    .setLatLng(e.latlng)
    .setContent("Posi&ccedil;&atilde;o: " + e.latlng.toString())
    .openOn(regosh);
}

regosh.on('click', onMapClick);

/* @preserve @license-end */
